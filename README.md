**Test task for RIA**
*Selenide/Maven/Hamcrest/Allure/TestNG*
---

## Run test

1. git clone https://y0ga@bitbucket.org/y0ga/riatest.git
2. mvn test
3. allure generate

---

## Open results

Allure Report will be generated in /allure-report directory.
Open index.html

---