package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import java.util.List;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static pages.ElementLocators.*;

public class AutoRiaSearchPage {

    @Step("Accept Cookies")
    public AutoRiaSearchPage closeCookieNotifier() {
        $(acceptCookies).click();
        return this;
    }

    @Step("Click B/U cars")
    public AutoRiaSearchPage chooseUsedCars() {
        $(usedCarsCheckbox).click();
        return this;
    }

    @Step("Click Verified VIN")
    public AutoRiaSearchPage chooseVerifiedVin() {
        $(verifiedVinCheckbox).click();
        return this;
    }

    @Step("Pick from category {category}")
    public AutoRiaSearchPage chooseCategory(String category) {
        $(categoryPicker).selectOptionContainingText(category);
        return this;
    }

    @Step("Pick from brand {brand}")
    public AutoRiaSearchPage chooseBrand(String brand) {
        $(brandInput).setValue(brand);
        $(byAttribute("data-text", brand)).click();
        return this;
    }

    @Step("Press search")
    public void pressSearch() {
        $(searchButton).click();
    }

    @Step("Get adds titles from search result")
    public List<String> getAddsTitles() {
        $(searchResults).waitUntil(Condition.visible, 10000);
        return $$(addResultLabels).texts();
    }

    @Step("Get adds prices from search result")
    public ElementsCollection getAddsPrices() {
        $(searchResults).waitUntil(Condition.visible, 10000);
        return $$(addResultUsdPrices);
    }

    @Step("Set price FROM:{from} TO:{to}")
    public AutoRiaSearchPage setPriceRange(String from, String to) {
        $(inputPriceFrom).setValue(from);
        $(inputPriceTo).setValue(to);
        return this;
    }

    @Step("Get price FROM input value")
    public String getPriceFrom() {
        return $(inputPriceFrom).getValue();
    }

    @Step("Get price TO input value")
    public String getPriceTo() {
        return $(inputPriceTo).getValue();
    }
}
