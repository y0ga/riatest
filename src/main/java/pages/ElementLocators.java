package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;

class ElementLocators {

    static By acceptCookies = byAttribute("for", "c-notifier-close");
    static By usedCarsCheckbox = byAttribute("for", "indexName_bu");
    static By verifiedVinCheckbox = byAttribute("for", "verifiedVIN");
    static By categoryPicker = byId("category");
    static By brandInput = byId("brandTooltipBrandAutocompleteInput-0");
    static By searchButton = byId("floatingSearchButton");
    static By searchResults = byId("searchResults");
    static By addResultLabels = byXpath("//span[@class='blue bold']");
    static By addResultUsdPrices = byXpath("//div[@class='price-ticket']/span[@class='size15']/span[@data-currency='USD']");
    static By inputPriceFrom = byName("price.USD.gte");
    static By inputPriceTo = byName("price.USD.lte");

}
