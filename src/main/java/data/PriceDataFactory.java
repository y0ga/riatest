package data;

import org.testng.annotations.DataProvider;

public class PriceDataFactory {

    @DataProvider(name = "data-provider-positive")
    public Object[][] dataProviderPositive() {
        return new Object[][]{
                {"1", "6000"}, {"10", "100000"},
                {"1000", "100000"}, {"15000", "50000"}
        };
    }

    @DataProvider(name = "data-provider-negative")
    public Object[][] dataProviderNegative() {
        return new Object[][]{
                {"AAA", "AAA"}, {"aaA", "AAaa"},
                {"Q!@#", "@#@##@"}, {"**&)", "_))(--+="}
        };
    }

}
