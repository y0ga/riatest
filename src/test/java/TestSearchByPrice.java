import data.PriceDataFactory;
import org.testng.annotations.BeforeClass;
import pages.AutoRiaSearchPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.StringContains.containsString;

public class TestSearchByPrice extends TestBase {

    private AutoRiaSearchPage searchPage;

    @BeforeClass
    public void setUpClass() {
        searchPage = open("/search", AutoRiaSearchPage.class)
                .closeCookieNotifier()
                .chooseUsedCars()
                .chooseVerifiedVin()
                .chooseCategory("Легковые");
    }

    @Test(description = "Check that filtering by Used, VIN, Category and Brand is working")
    void testFilterByUsageVinBrand() {
        String brand = "Toyota";
        searchPage.chooseBrand(brand)
                .pressSearch();

        searchPage.getAddsTitles().forEach(
                s -> assertThat(s, containsString(brand))
        );
    }

    @Test(description = "Check that filtering by price is working with positive price data",
            dataProviderClass = PriceDataFactory.class, dataProvider = "data-provider-positive")
    void testPositivePriceInput(String from, String to) {
        Integer f = Integer.parseInt(from);
        Integer t = Integer.parseInt(to);

        searchPage.setPriceRange(from, to)
                .pressSearch();

        searchPage.getAddsPrices().stream().limit(5).forEach(
                s -> {
                    String crop = s.getText().replaceAll(" ", "");
                    Integer i = Integer.parseInt(crop);
                    assertThat(i, allOf(greaterThan(f - 1), lessThan(t + 1)));
                }
        );
    }

    @Test(description = "Check that filtering by price is working correct with negative data",
            dataProviderClass = PriceDataFactory.class, dataProvider = "data-provider-negative")
    void testNegativePriceInput(String from, String to) {
        searchPage.setPriceRange(from, to);
        assertThat(searchPage.getPriceFrom(), isEmptyString());
        assertThat(searchPage.getPriceTo(), isEmptyString());
    }
}
