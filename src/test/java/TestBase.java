import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;

public class TestBase {

    @BeforeTest
    public static void setUpEnv() {
        Configuration.baseUrl = "https://auto.ria.com";
        Configuration.timeout = 10000;
        Configuration.startMaximized = true;
        Configuration.headless = false;
        Configuration.fastSetValue = true;
    }

}
